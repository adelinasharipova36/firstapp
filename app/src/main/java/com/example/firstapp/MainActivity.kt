package com.example.firstapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.firstapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            btnChangeImage.setOnClickListener {
                ivRandomImage.setImageResource(images.random())
                btnChangeImage.setBackgroundResource(colors.random())
            }
        }
    }

    private companion object {

        val images = arrayOf(
            R.drawable.ic_first,
            R.drawable.ic_second,
            R.drawable.ic_third,
            R.drawable.ic_fourth,
            R.drawable.ic_fifth,
            R.drawable.ic_sixth
        )

         val colors = arrayOf(
            R.color.purple_200,
            R.color.black,
            R.color.purple_500,
            R.color.purple_700,
            R.color.teal_700,
            R.color.teal_200
        )
    }
}